import React, { useState } from "react";

export default function App() {
  const questions = [
    {
      questionText: "What OS do you want to target?",
      answerOptions: [
        { answerText: "iOs", isCorrect: 60000 },
        { answerText: "Android", isCorrect: false },
        { answerText: "Web", isCorrect: true }
      ]
    },
    {
      questionText: "How big is your app?",
      answerOptions: [
        { answerText: "Small", isCorrect: false },
        { answerText: "Medium", isCorrect: true },
        { answerText: "Large", isCorrect: false }
      ]
    },
    {
      questionText: "What level of UI would you like?",
      answerOptions: [
        { answerText: "Minimum viable product", isCorrect: true },
        { answerText: "Basic", isCorrect: false },
        { answerText: "Custom", isCorrect: false }
      ]
    },
    {
      questionText: "What kind of sign up feature do you want?",
      answerOptions: [
        { answerText: "Email/password", iscorrect: false },
        { answerText: "Social media account", isCorrect: false },
        { answerText: "No sign up ", isCorrect: false }
      ]
    },
    {
      questionText: "Does your app need any upload feature?",
      answerOptions: [
        { answerText: "Photo upload", isCorrect: false },
        { answerText: "Video upload", isCorrect: false },
        { answerText: "Both", isCorrect: false },
        { answerText: "no upload feature", isCorrect: false }
      ]
    },
    {
      questionText: "What set of functionality do you want in your app?",
      answerOptions: [
        { answerText: "Map & Geo Location", isCorrect: false },
        { answerText: "Social Share", isCorrect: false },
        { answerText: "Audio/Video Player", isCorrect: false },
        { answerText: "News/Social feeds", isCorrect: false }
      ]
    },
    {
      questionText: "What set of functionality do you want in your app?(ii)",
      answerOptions: [
        { answerText: "Push Notification", isCorrect: false },
        { answerText: "Bluetooth Connectivity", isCorrect: false },
        { answerText: "In App Notification", isCorrect: false },
        { answerText: "In App purchases", isCorrect: false }
      ]
    },
    {
      questionText: "What level of security do you need?",
      answerOptions: [
        { answerText: "Basic", isCorrect: false },
        { answerText: "Encryption", isCorrect: false },
        {
          answerText: "Ultra protection(ex.financial transactions)",
          isCorrect: false
        },
        { answerText: "Not sure", isCorrect: false }
      ]
    }
  ];
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const handleNextButtonClick = (answerOption) => {
    const nextQuestion = currentQuestion + 1;
    setCurrentQuestion(nextQuestion);
  };
  const handleAnswerButtonClick = () => {
    document.getElementById("clickedButton").style.backgroundColor = "#555e7d";
  };
  return (
    <div className="app">
      {/* HINT: replace "false" with logic to display the 
      score when the user has answered all the questions */}
      {false ? (
        <div className="score-section">
          You scored 1 out of {questions.length}
        </div>
      ) : (
        <>
          <div className="question-section">
            <div className="question-count">
              <span>Question {currentQuestion + 1}</span>/{questions.length}
            </div>
            <div className="question-text">
              {questions[currentQuestion].questionText}
            </div>
          </div>
          <div className="answer-section">
            {questions[currentQuestion].answerOptions.map(
              (answerOption, index) => (
                <button
                  id="clickedButton"
                  onClick={() => handleAnswerButtonClick()}
                >
                  {answerOption.answerText}
                </button>
              )
            )}
          </div>
        </>
      )}
      <div className="next-button">
        <button onClick={() => handleNextButtonClick()}>next</button>
      </div>
    </div>
  );
}
